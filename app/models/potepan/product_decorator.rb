module Potepan::ProductDecorator
  def related_products
    Spree::Product.joins(:taxons).where('spree_taxons.id': taxons.ids).
      where.not('spree_products.id': id).
      order(Arel.sql('min(spree_taxons.id)')).group('spree_products.id').
      includes(master: [:images, :default_price])
  end

  Spree::Product.prepend self
end
