class Potepan::ProductsController < ApplicationController
  MAXIMUM_DISPLAYED_PRODUCTS = 4
  def show
    @product = Spree::Product.find(params[:id])
    @taxon_id = @product.taxon_ids.first || Spree::Taxon.first.id
    @related_products = @product.related_products.limit(MAXIMUM_DISPLAYED_PRODUCTS)
  end
end
