require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "#full_title by helpers" do
    context "no argument is found" do
      it "returns the default title" do
        expect(full_title).to eq 'BIGBAG Store'
      end
    end

    context "an argument is empty" do
      it "returns the default title" do
        expect(full_title('')).to eq 'BIGBAG Store'
      end
    end

    context "an argument is nil" do
      it "returns the default title" do
        expect(full_title(nil)).to eq 'BIGBAG Store'
      end
    end

    context "an argument is assigned" do
      it "returns the title with an argument" do
        expect(full_title('Test')).to eq 'Test - BIGBAG Store'
      end
    end
  end
end
