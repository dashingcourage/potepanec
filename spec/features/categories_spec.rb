require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let(:taxonomy) { create :taxonomy, name: "Taxons" }
  let(:jackets_taxon) { taxonomy.root.children.create(name: "Jackets") }
  let(:shoes_taxon) { taxonomy.root.children.create(name: "Shoes") }
  let!(:jackets_product) do
    create :product, name: "Ruby Jacket",
                     price: "1230.45", taxons: [jackets_taxon]
  end
  let!(:shoes_product) do
    create :product, name: "Ruby Shoes",
                     price: "150.00", taxons: [shoes_taxon]
  end

  describe "#show by features" do
    context "when viewing products of all categories" do
      before do
        visit potepan_category_path(taxonomy.root.id)
      end

      it "can move to the index page from the light_top_section" do
        within "ol.breadcrumb" do
          expect(page).to have_link 'Home', href: potepan_path
          click_link 'Home', href: potepan_path
        end
        expect(page).to have_current_path(potepan_path)
      end

      it "can go to a show page of Jackets from the category filter" do
        within "div.panelCategory" do
          click_link 'Taxons'
          expect(page).to have_link 'Jackets', href: potepan_category_path(jackets_taxon.id)
          click_link 'Jackets', href: potepan_category_path(jackets_taxon.id)
        end
        expect(page).to have_current_path(potepan_category_path(jackets_taxon.id))
        within "div.page-title" do
          expect(page).to have_selector 'h2', text: jackets_taxon.name
        end
      end

      it "can view a show page of all taxonomy.root.products " do
        within "div.page-title" do
          expect(page).to have_selector 'h2', text: taxonomy.root.name
        end
        within "div.productRow" do
          expect(page).to have_selector 'h5', text: jackets_product.name
          expect(page).to have_selector 'h3', text: jackets_product.display_price.to_s
          expect(page).to have_selector 'h5', text: shoes_product.name
          expect(page).to have_selector 'h3', text: shoes_product.display_price.to_s
        end
      end

      it "can visit the shoes_product page" do
        within "div.productRow" do
          click_link shoes_product.name
        end
        expect(page).to have_current_path(potepan_product_path(shoes_product.id))
        within "div.page-title" do
          expect(page).to have_selector 'h2', text: shoes_product.name
        end
      end
    end

    context "when viewing products of Jackets" do
      before do
        visit potepan_category_path(jackets_taxon.id)
      end

      it "can move to the index page from the light_top_section" do
        within "ol.breadcrumb" do
          expect(page).to have_link 'Home', href: potepan_path
          click_link 'Home', href: potepan_path
        end
        expect(page).to have_current_path(potepan_path)
      end

      it "can go to a show page of Shoes from the category filter" do
        within "div.panelCategory" do
          click_link 'Taxons'
          expect(page).to have_link 'Shoes', href: potepan_category_path(shoes_taxon.id)
          click_link 'Shoes', href: potepan_category_path(shoes_taxon.id)
        end
        expect(page).to have_current_path(potepan_category_path(shoes_taxon.id))
        within "div.page-title" do
          expect(page).to have_selector 'h2', text: shoes_taxon.name
        end
      end

      it "can view a show page of only jackets_taxon products" do
        within "div.page-title" do
          expect(page).to have_selector 'h2', text: jackets_taxon.name
        end
        within "div.productRow" do
          expect(page).to have_selector 'h5', text: jackets_product.name
          expect(page).to have_selector 'h3', text: jackets_product.display_price.to_s
          expect(page).not_to have_selector 'h5', text: shoes_product.name
          expect(page).not_to have_selector 'h3', text: shoes_product.display_price.to_s
        end
      end

      it "can visit the jackets_product page" do
        within "div.productRow" do
          click_link jackets_product.name
        end
        expect(page).to have_current_path(potepan_product_path(jackets_product.id))
        within "div.page-title" do
          expect(page).to have_selector 'h2', text: jackets_product.name
        end
      end
    end
  end
end
