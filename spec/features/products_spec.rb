require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:taxonomy) { create :taxonomy, name: "Taxons" }
  let(:taxon) { create :taxon, name: "Watch", parent: taxonomy.root, taxonomy: taxonomy }
  let(:rubyish_watch) do
    create :product, name: "Rubyish Watch", price: "50.00", taxons: [taxon]
  end
  let!(:potepanish_watch) do
    create :product, name: "Potepanish Watch", price: "123.45", taxons: [taxon]
  end
  let!(:diamondish_watch) do
    create :product, name: "Diamondish Watch", price: "12300.45", taxons: [taxon]
  end
  let!(:perlish_watch) do
    create :product, name: "Perlish Watch", price: "500.00", taxons: [taxon]
  end
  let(:rubyish_watch_image) { build(:image) }

  before do
    rubyish_watch.images << rubyish_watch_image
  end

  describe "#show by features" do
    before do
      visit potepan_product_path(rubyish_watch.id)
    end

    it "can move to the index page from the light_top_section" do
      within "ol.breadcrumb" do
        expect(page).to have_link 'Home', href: potepan_path
        click_link 'Home', href: potepan_path
      end
      expect(page).to have_current_path(potepan_path)
    end

    it "can go to a show page of the taxon 'Watch'" do
      within "div.media-body" do
        expect(page).to have_link '一覧ページへ戻る',
                                  href: potepan_category_path(rubyish_watch.taxon_ids.first)
        click_link '一覧ページへ戻る', href: potepan_category_path(rubyish_watch.taxon_ids.first)
      end
      expect(page).to have_current_path(potepan_category_path(rubyish_watch.taxon_ids.first))
      within "div.page-title" do
        expect(page).to have_selector 'h2', text: rubyish_watch.taxons.first.name
      end
    end

    it "can view a show page of the product 'Rubyish Watch'" do
      within "div.page-title" do
        expect(page).to have_selector 'h2', text: rubyish_watch.name
      end
      within "div.item" do
        expect(page).to have_css("img[src*='thinking-cat.jpg']")
      end
      within "div.thumb" do
        expect(page).to have_css("img[src*='thinking-cat.jpg']")
      end
      within "div.media-body" do
        expect(page).to have_selector 'h2', text: rubyish_watch.name
        expect(page).to have_selector 'h3', text: rubyish_watch.display_price.to_s
        expect(page).to have_selector 'p', text: rubyish_watch.description
      end
    end

    it "can view related products of the product 'Rubyish Watch'" do
      within "div.productsContent" do
        expect(page).to have_selector 'h5', text: potepanish_watch.name
        expect(page).to have_selector 'h3', text: potepanish_watch.display_price.to_s
        expect(page).to have_selector 'h5', text: diamondish_watch.name
        expect(page).to have_selector 'h3', text: diamondish_watch.display_price.to_s
        expect(page).to have_selector 'h5', text: perlish_watch.name
        expect(page).to have_selector 'h3', text: perlish_watch.display_price.to_s
        expect(page).not_to have_selector 'h5', text: rubyish_watch.name
        expect(page).not_to have_selector 'h3', text: rubyish_watch.display_price.to_s
      end
    end
  end
end
