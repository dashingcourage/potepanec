require 'rails_helper'

RSpec.describe "Products", type: :request do
  describe "#show by requests" do
    let(:taxon) { create :taxon }
    let(:potepan_watch) do
      create :product, name: "Potepan Watch", price: "123.45", taxons: [taxon]
    end
    let!(:ruby_watch) do
      create :product, name: "Ruby Watch", price: "100.00", taxons: [taxon]
    end
    let!(:diamond_watch) do
      create :product, name: "Diamond Watch", price: "12300.45", taxons: [taxon]
    end
    let!(:perl_watch) do
      create :product, name: "Perl Watch", price: "500.00", taxons: [taxon]
    end

    context "when visiting potepan_watch's page" do
      before do
        get potepan_product_path(potepan_watch.id)
      end

      it "returns successfully" do
        expect(response).to be_successful
      end

      it "shows a name of Potepan Watch" do
        expect(response.body).to include(potepan_watch.name)
      end

      it "includes a price of Potepan Watch" do
        expect(response.body).to include(potepan_watch.display_price.to_s)
      end

      it "shows a name of Ruby Watch" do
        expect(response.body).to include(ruby_watch.name)
      end

      it "includes a price of Ruby Watch" do
        expect(response.body).to include(ruby_watch.display_price.to_s)
      end

      it "shows a name of Diamond Watch" do
        expect(response.body).to include(diamond_watch.name)
      end

      it "includes a price of Diamond Watch" do
        expect(response.body).to include(diamond_watch.display_price.to_s)
      end

      it "shows a name of Perl Watch" do
        expect(response.body).to include(perl_watch.name)
      end

      it "includes a price of Perl Watch" do
        expect(response.body).to include(perl_watch.display_price.to_s)
      end
    end
  end
end
