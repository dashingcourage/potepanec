require 'rails_helper'

RSpec.describe "Categories", type: :request do
  describe "#show by requests" do
    let(:taxonomy) { create :taxonomy }
    let(:jackets_taxon) { taxonomy.root.children.create(name: "Jackets") }
    let(:shoes_taxon) { taxonomy.root.children.create(name: "T-Shirts") }
    let!(:jackets_product) do
      create :product, name: "Potepan Jacket",
                       price: "123.45", taxons: [jackets_taxon]
    end
    let!(:shoes_product) do
      create :product, name: "Potepan Shoes",
                       price: "50.00", taxons: [shoes_taxon]
    end

    context "when viewing products of all categories" do
      before do
        get potepan_category_path(taxonomy.root.id)
      end

      it "returns successfully" do
        expect(response).to be_successful
      end

      it "shows a name of Potepan Jacket" do
        expect(response.body).to include(jackets_product.name)
      end

      it "includes a price of Potepan Jacket" do
        expect(response.body).to include(jackets_product.display_price.to_s)
      end

      it "shows a name of Potepan Shoes" do
        expect(response.body).to include(shoes_product.name)
      end

      it "includes a price of Potepan Shoes" do
        expect(response.body).to include(shoes_product.display_price.to_s)
      end
    end

    context "when viewing products of Jackets" do
      before do
        get potepan_category_path(jackets_taxon.id)
      end

      it "returns successfully" do
        expect(response).to be_successful
      end

      it "shows a name of Potepan Jacket" do
        expect(response.body).to include(jackets_product.name)
      end

      it "includes a price of Potepan Jacket" do
        expect(response.body).to include(jackets_product.display_price.to_s)
      end

      it "does not show a name of Potepan Shoes" do
        expect(response.body).not_to include(shoes_product.name)
      end

      it "does not include a price of Potepan Shoes" do
        expect(response.body).not_to include(shoes_product.display_price.to_s)
      end
    end
  end
end
